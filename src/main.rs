#![feature(box_patterns, test)]

extern crate test;

use std::{
    fmt,
    str::FromStr,
    convert::TryFrom,
};
use Lambda::*;

#[derive(Debug, Clone, Eq)]
enum Lambda {
    Var(u64),
    Abs(Box<Lambda>),
    App(Box<Lambda>, Box<Lambda>),
}

impl Lambda {
    fn reduct(self) -> Self {
        match self {
            App(box Abs(a), box b) => a.substitute(1, b),
            App(a, b) => App(
                Box::new(a.reduct()),
                Box::new(b.reduct()),
            ),
            Abs(a) => Abs(
                Box::new(a.reduct()),
            ),
            _ => self
        }
    }

    fn substitute(self, binders: u64, x: Self) -> Self {
        match self {
            Var(a) if a == binders => x.inc_fv(0, binders - 1),
            Var(a) if a > binders => Var(a - 1), // Free variables
            Abs(a) => Abs(
                Box::new(a.substitute(binders + 1, x)),
            ),
            App(a, b) => App(
                Box::new(a.substitute(binders, x.clone())),
                Box::new(b.substitute(binders, x)),
            ),
            _ => self
        }
    }

    fn inc_fv(self, binders: u64, n: u64) -> Self {
        match self {
            Var(a) if a > binders => Var(a + n),
            Abs(a) => Abs(
                Box::new(a.inc_fv(binders + 1, n)),
            ),
            App(a, b) => App(
                Box::new(a.inc_fv(binders, n)),
                Box::new(b.inc_fv(binders, n)),
            ),
            _ => self
        }
    }

    fn done(&self) -> bool {
        match self {
            App(box Abs(_), _) => false,
            App(a, b) => a.done() && b.done(),
            Abs(a) => a.done(),
            _ => true
        }
    }

    fn reduct_max(self) -> Self {
        let mut lam = self;
        while !&lam.done() {
            lam = lam.reduct();
        }
        lam
    }
}

impl PartialEq for Lambda {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Var(a), Var(b)) if a == b => true,
            (Abs(a), Abs(b)) => a.eq(b),
            (App(a, b), App(c, d)) => a.eq(c) && b.eq(d),
            _ => false
        }
    }
}

impl fmt::Display for Lambda {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Var(a) => write!(f, "{}", a),
            Abs(a) => write!(f, "(λ {})", a),
            App(a, b) => write!(f, "({} {})", a, b),
        }
    }
}

impl FromStr for Lambda {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars();
        match chars.next() {
            Some('0' ..= '9') => {
                Ok(Var(s.chars().take_while(|a| a.is_ascii_digit())
                        .collect::<String>().parse::<u64>().unwrap()))
            },

            Some('(') => {
                let between = &s[1 .. s.len() - 1];
                match chars.next() {
                    Some('λ') => {
                        Ok(Abs(Box::new(Lambda::from_str(&between['λ'.len_utf8()+1..])?)))
                    },
                    Some('(') => {
                        let mut iter = between.chars();
                        let mut p = 0;
                        Ok(App(
                                Box::new(Lambda::from_str(&iter.by_ref()
                                        .take_while(|x| {
                                            match *x {
                                                '(' => { p += 1; true },
                                                ')' => { p -= 1; true },
                                                ' ' if p == 0 => false,
                                                _ => true
                                            }
                                        }).collect::<String>())?
                                ),
                                Box::new(Lambda::from_str(&iter.collect::<String>())?)
                        ))
                    },
                    Some(_) => {
                        let mut iter = between.chars();
                        Ok(App(
                                Box::new(Lambda::from_str(&iter.by_ref()
                                        .take_while(|x|
                                            *x != ' '
                                        ).collect::<String>())?
                                ),
                                Box::new(Lambda::from_str(&iter.collect::<String>())?)
                        ))
                    },
                    None => Err(String::from("Unmatched parenthese"))
                }
            },
            _ => Err(String::from("Not valid"))
        }
    }
}

impl From<u64> for Lambda {
    fn from(n: u64) -> Self {
        if n == 0 {
            Abs(Box::new(Abs(Box::new(Var(1)))))
        } else {
            App(
                Box::new(Abs(
                        Box::new(Abs(
                                Box::new(Abs(
                                        Box::new(App(
                                                Box::new(Var(2)),
                                                Box::new(App(
                                                        Box::new(App(
                                                                Box::new(Var(3)),
                                                                Box::new(Var(2)))),
                                                        Box::new(Var(1)))))))))))),
                Box::new(Lambda::from(n - 1)))
        }
    }
}

impl TryFrom<Lambda> for u64 {
    type Error = String;

    fn try_from(l: Lambda) -> Result<Self, Self::Error> {
        if let Abs(box Abs(box a)) = l.reduct_max() {

            fn depth(lam: Lambda) -> Result<u64, String> {
                match lam {
                    Var(1) => Ok(0),
                    App(box Var(2), box a) => Ok(1 + depth(a)?),
                    _ => Err(String::from("Not a Church numeral"))
                }
            }

            depth(a)
        } else {
            Err(String::from("Not a Church numeral"))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn int_to_lambda() {
        assert_eq!(Lambda::from(3).reduct_max(), Abs(
                Box::new(Abs(
                        Box::new(App(
                                Box::new(Var(2)),
                                Box::new(App(
                                        Box::new(Var(2)),
                                        Box::new(App(
                                                Box::new(Var(2)),
                                                Box::new(Var(1))))))))))));
    }

    #[test]
    fn lambda_to_int() {
        assert_eq!(Ok(2), u64::try_from(Abs(
                    Box::new(Abs(
                            Box::new(App(
                                    Box::new(Var(2)),
                                    Box::new(App(
                                            Box::new(Var(2)),
                                            Box::new(Var(1)))))))))));
    }

    // TODO: test failure

    #[test]
    fn lambda_to_str() {
        assert_eq!("(λ (λ ((2 1) 2)))", format!("{}", Abs(
                    Box::new(Abs(
                            Box::new(App(
                                    Box::new(App(
                                            Box::new(Var(2)),
                                            Box::new(Var(1)))),
                                    Box::new(Var(2)))))))));
    }

    #[test]
    fn str_to_lambda() {
        assert_eq!(Lambda::from_str("(λ (λ ((2 1) 2)))"), Ok(Abs(
                Box::new(Abs(
                            Box::new(App(
                                    Box::new(App(
                                            Box::new(Var(2)),
                                            Box::new(Var(1)))),
                                    Box::new(Var(2)))))))));
    }

    // TODO: test failure

    #[bench]
    fn six_times_seven(b: &mut Bencher) {
        b.iter(|| {
            let mult = Lambda::from_str("(λ (λ (λ (3 (2 1)))))").unwrap();
            assert_eq!(Ok(42), u64::try_from(App(
                        Box::new(App(
                                Box::new(mult),
                                Box::new(Lambda::from(6)))),
                        Box::new(Lambda::from(7))).reduct_max()));
        });
    }

    #[bench]
    fn factorial(b: &mut Bencher) {
        b.iter(|| {
            let y = "(λ ((λ (2 (1 1))) (λ (2 (1 1)))))";
            let ltrue = "(λ (λ 2))";
            let lfalse = "(λ (λ 1))";
            let is_zero = format!("(λ ((1 (λ {})) {}))", lfalse, ltrue);
            let mult = "(λ (λ (λ (3 (2 1)))))";
            let pred = "(λ (λ (λ (((3 (λ (λ (1 (2 4))))) (λ 2)) (λ 1)))))";
            let h = format!("(λ (λ ((({} 1) {}) (({} 1) (2 ({} 1))))))",
                is_zero, Lambda::from(1), mult, pred);
            let fac = format!("({} {})", y, Lambda::from_str(&h).unwrap().reduct_max());
            assert_eq!(Ok(6), u64::try_from(App(
                        Box::new(Lambda::from_str(&fac).unwrap()),
                        Box::new(Lambda::from(3).reduct_max())).reduct_max()));
        });
    }

    #[bench]
    fn lambda_from_10(b: &mut Bencher) {
        b.iter(|| Lambda::from(10).reduct_max());
    }
}

// TODO: Multithreading
// TODO: Optimization
// FIXME: Stack overflow
// TODO: repl with define (:=)
// TODO: Reduce verbosity with less parentheses

fn main() {
}
